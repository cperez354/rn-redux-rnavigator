import { bindActionCreators } from 'redux';
import * as milkActionCreators from '../containers/Milk/ActionsCreators';
import * as anotherActionCreators from '../containers/Another/ActionsCreators';

const ActionsCreators = {
  ...milkActionCreators,
  ...anotherActionCreators,
};

console.log(ActionsCreators);


export default mapDispatchToProps = function(dispatch){
  return bindActionCreators(ActionsCreators, dispatch);
}