
import { combineReducers, createStore, compose } from 'redux';
import rootReducer from './rootReducer';

const defaultState = {
    milk: 0,
    realcount: { count: 0 }
};

export default createStore(rootReducer, defaultState);