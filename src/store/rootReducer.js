import { combineReducers } from 'redux';
import milkReducer from '../containers/Milk/Reducer';
import anotherReducer from '../containers/Another/Reducer';

export default combineReducers({
    milk: milkReducer,
    realcount: anotherReducer,
});