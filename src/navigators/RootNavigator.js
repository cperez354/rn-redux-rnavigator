import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';

import MilkStash from '../containers/Milk/MilkStash'
import Another from '../containers/Another/Another';
import AnotherMore from '../containers/AnotherMore/AnotherMore';
import mapStateToProps from '../store/mapStateToProps';
import mapDispatchToProps from '../store/mapDispatchToProps';


const { log } = console;
const connectionStore = connect(mapStateToProps, mapDispatchToProps);

/* const mapStateToProps = (state, props) => ({
  ...navigation.state.params
}); */

const mapNavigationStateParamsToProps = (SomeComponent) => {
  return class extends Component {
      static navigationOptions = SomeComponent.navigationOptions; // better use hoist-non-react-statics
      render() {
          const { navigation: { state: { params } } } = this.props
          log('NAVIGATOR :::', {...this.props} );
          return <SomeComponent {...params} {...this.props} />
      }
  }
}

const paramsToProps = (SomeComponent) => { 
  // turns this.props.navigation.state.params into this.params.<x>
      return class extends Component {
          render() {
              const {navigation, ...otherProps} = this.props
              const {state: {params}} = navigation
              return <SomeComponent {...this.props} {...params} />
          }
      }
  }

const RootNavigator = StackNavigator(
  { Home: { screen: mapNavigationStateParamsToProps(MilkStash) },
    Another: { screen: mapNavigationStateParamsToProps(Another) },
    AnotherMore: { screen: mapNavigationStateParamsToProps(AnotherMore) }
  },
);

export default (connectionStore)(RootNavigator);