import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Button} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import styles from './MilkStashStyle';

import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

export class MilkStash extends Component {
  constructor(props){
    super(props);
  }

  static navigationOptions = {
    title: 'Welcome',
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
          <Button
                onPress={ () => this.props.addMilk()}
                title="Dispatch"
                color="#841584">
          </Button>
          <Text> Number :
          {
            this.props.milk
          }
          </Text>
          <Button
              title="Go to Jane's profile"
              onPress={() =>
                navigate('Another')
              }
            />
      </View>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MilkStash);