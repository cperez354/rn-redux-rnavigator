import Actions from '../../actions/actions';

// ADD
export const addMilk = (count) => {
    return {
      type: Actions.ADD_MILK,
      count
    };
}

// SUB
export const subMilk = (count) => {
  console.log('ENTER ACTION CREATOR ANOTHER');
  return {
    type: Actions.SUB_MILK,
    count
  };
}