import Action from '../../actions/actions';

export default (state = 0, action = {}) => {
    console.log('ENTER TO REDUCER MILK');
    console.log(action.type);
    console.log(state);
    switch (action.type) {
        case Action.ADD_MILK:
            return state + 1;
        case Action.SUB_MILK:
            return state - 1;
        default:
            return state;
    }
    return state;
}