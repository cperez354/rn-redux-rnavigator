import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import NoConnected from '../../components/NoConnected/NoConnected';
import styles from './AnotherStyle';

import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

class Another extends Component {
  constructor(props){
    super(props);
  }
  render() {
    console.log(this.props);
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
          <Text>Another</Text>
          <Button
                onPress={ () => this.props.addMilk()}
                title="Dispatch"
                color="#841584">
          </Button>
          <Text> Number :
          {
            this.props.milk
          }
          </Text>
          <Button
                onPress={ () => this.props.subMilk()}
                title="SUB-MILK DISPATCH"
                color="#841584">
          </Button>
          <Text> REALCOUNT :
          {
            this.props.realcount.count
          }
          </Text>
          <Button
                onPress={ () => this.props.addAnother()}
                title="ANOTHER DISPATCH"
                color="#841584">
          </Button>
          <Button
              title="Go to Jane's profile"
              onPress={() =>
                navigate('AnotherMore')
              }
            />
          <NoConnected {...this.props} ></NoConnected>
      </View>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Another);