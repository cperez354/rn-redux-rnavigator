import React, { Component } from 'react';
import { Provider } from 'react-redux';
import RootNavigator from './src/navigators/RootNavigator';
import store from './src/store/store';

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <RootNavigator { ...this.props } />
      </Provider>)
  }
}

export default App;
